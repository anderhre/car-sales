import Cars from './models/cars';

export const PAGESIZE: number = 20; // Amount of cars on each page

export default {
  Query: {
    cars: async (_, { filter, sort }) => {
      let pageSkip = 0;
      let insertFilter = {}

      // Create filter to insert if filter is specified
      if (filter != null) {
        insertFilter = {
          year: {
            $gte: filter.minyear,
            $lte: filter.maxyear,
          },
          brand: { $regex: filter.brand, $options: 'i' },
          model: { $regex: filter.model, $options: 'i' },
        };

        // Set the amount of pages to skip based on the page specified
        if (filter.page !== 0) {
          pageSkip += filter.page - 1;
        }
      }
      // Calculate final page
      let finalPage = await calculateFinalPage(insertFilter);

      // If sort is specified
      if (sort != null) {
        // If pageskip is negative, return cars on the final page
        if (pageSkip < 0) {
          // Find cars on final page
          let cars = Cars.find(insertFilter)
            .sort({ [sort.field]: sort.order === 'ASC' ? 1 : -1 })
            .skip((finalPage - 1) * PAGESIZE)
            .limit(PAGESIZE);

          return {
            cars: cars,
            finalpage: finalPage,
          };
        }
        // Pageskip is not negative, return cars on the page specified
        let cars = Cars.find(insertFilter)
          .sort({ [sort.field]: sort.order === 'ASC' ? 1 : -1 })
          .skip(pageSkip * PAGESIZE)
          .limit(PAGESIZE);

        return {
          cars: cars,
          finalpage: finalPage,
        };
      }
      // Sort is not specified, return cars on specified page and sort ascendingly on brand
      let cars = Cars.find(insertFilter)
        .sort({ brand: 1 })
        .skip(pageSkip * PAGESIZE)
        .limit(PAGESIZE);

      return {
        cars: cars,
        finalpage: finalPage,
      };
    },
  },
  Mutation: {
    like: async (_, { id, likeAmount }) => {
      // Increment field "likes" by likeAmount, on a car with the specified ID
      try {
        await Cars.updateOne({ index: id }, { $inc: { likes: likeAmount } });
      } catch (e) {
        return { success: false };
      }
      
      return { success: true };
    },
  },
};
// Calculate number of final page with a given filter
async function calculateFinalPage(insertFilter: any) {
  let carAmount: number = await Cars.find(insertFilter).countDocuments();
  let finalPage = Math.ceil(carAmount / PAGESIZE);
  
  return finalPage;
}