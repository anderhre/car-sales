const fs = require('fs');
const csv = require('csv-parser');
const mongoose = require('mongoose');

//Connects to the database with admin rights
function insertAll() {
  const uri = 'mongodb://localhost:27017/cars_db';
  mongoose
    .connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
      connected();

      const data = [];

      //Drops table cars
      mongoose.connection.db.dropCollection('cars', function(err, result) {
        if (err) {
          console.log("Drop collection 'cars': " + result + '\n' + err);
        } else          {
          console.log("Drop collection 'cars': ");
          console.log(result);
        }
      });

      const Cars = mongoose.model('Cars', {
        index: { type: Number },
        price: { type: Number },
        model: { type: String },
        brand: { type: String },
        year: { type: Number },
        title_status: { type: String },
        mileage: { type: Number },
        color: { type: String },
        vin: { type: String },
        lot: { type: Number },
        state: { type: String },
        country: { type: String },
        condition: { type: String },
        likes: { type: Number },
      });

      //Creates list of cars from cvs-file.
      fs.createReadStream('./server/USA_cars_datasets.csv')
        .pipe(csv())
        .on('data', row => {
          // Convert to number
          row.index = +row.index;
          row.price = +row.price;
          row.year = +row.year;
          row.mileage = +row.mileage;
          row.lot = +row.lot;
          row.likes = +row.likes;

          data.push(row);
        })
        .on('end', () => {
          //Inserts data in collection
          Cars.insertMany(data)
            .then(function() {
              console.log('Data inserted successfully');
            })
            .catch(function(error) {
              console.log('Error, did not insert into database');
              console.log(error);
            });
        });
    })
    .catch(e => {
      console.log('Connection to database failed.\n', e.message);
    });
}
function connected() {
  console.log('MongoDB connected successfully');
}

insertAll();
