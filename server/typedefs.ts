// Sorting inspiration from: https://www.youtube.com/watch?v=dDxUu-K2qdE&t=250s&ab_channel=ApolloGraphQL
export default `
    type Car {
        index: Int!,
        price: Int!,
        model: String!,
        brand: String!,
        year: Int,
        title_status: String,
        mileage: Int,
        color: String,
        vin: String,
        lot: Int,
        state: String,
        country: String,
        condition: String,
        likes: Int
    }
    enum SortableField {
        index
        brand
        model
        price
        year
        likes
    }
    enum SortOrder {
        ASC
        DESC
    }
    input Sort {
        field: SortableField!
        order: SortOrder = ASC
    }
    input Filter {
        minyear: Int = 0
        maxyear: Int = 2020
        brand: String = ""
        model: String = ""
        page: Int = 1
    }

    type CarReply {
        cars: [Car!]
        finalpage: Int!
    }
    type LikeReply {
        success: Boolean!
    }
    
    type Query {
        cars(filter: Filter sort: Sort): CarReply!
    }
    type Mutation {
        like(id: Int!, likeAmount: Int!): LikeReply!
    }
    `;