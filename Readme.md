This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Table of Contents
[[_TOC_]]

# How to run

## Required software
### Node
* Download and install [Node.js](https://nodejs.org/en/download/).

### MongoDB
* Download and install [MongoDB Community Server](https://www.mongodb.com/try/download/community) and remember the installation directory.
* Create folders ```data/db``` in C-drive, these are needed for MongoDB to start.
* Navigate to the directory and run ```mongod.exe```. This needs to run to be able to connect to the database.

## Install & run project
* Make sure ```mongod.exe``` is running in a separate terminal as described above.
* From root (```Car sales```) in another terminal, run:
```bash
# Install npm-packages:
npm install
cd ./client
npm install
cd ../server
npm install
cd ../

# Set up and fill database (`mongod` has to be running in a separate terminal):
node ./server/fillDB.ts

# Start project (`mongod` has to be running in separate terminal):
npm run dev
```
Other commands:
```bash
# Run server and client separately, in two terminals:
npm run server
npm run client

# Run tests from the client-folder:
npm test

# Run visual cypress-test from the client-folder:
npm run cypress # then click on 'clientTest.ts'
```

# General
This project revolves around a React-client communicating with a local Apollo-express server with a MongoDB-database.

## Dataset
The dataset revolves around cars for sale, and is taken from [Kaggle](https://www.kaggle.com/doaaalsenani/usa-cers-dataset).

We added a column "likes" and inserted 0 in all rows.

## Functionality
### Client
The client can see all cars for sale, or filter by brand, model and/or model year. 
Cars will be displayed in a table with brand, model and price as the column headers.

Clicking on the column-headers will trigger a sort on the relevant column, clicking it again will sort in the opposite order.

To reduce loading time and stress on the server/database, the client fetches 20 cars at a time, this is solved through pagination.

When cars are displayed in the table, buttons for changing page will also be displayed at the bottom of the page.
The client can change page to the next and previous pages, but also to the first and final page.
If the client is on the first page, buttons for previous and first page are not displayed.
If the client is on the final page, buttons for next and final page are not displayed.

When a car or tablerow is clicked, a modal presents all the information about the car. In the modal the client can like and unlike, and see how many likes the car has received.

### Server and database
All filtration and sorting is handled by the server and database.

Sorting is done on the whole dataset to ensure that the client get the correct results.

# Structure

- <b>App</b>
    - <b>DataStoreProvider</b>  <i>(MobX: darhtheme)</i>
        - <b>ApolloProvider</b> <i>(for ApolloClient)</i>
            - <b>NavBar</b>
            - <b>CarList</b>
                - <b>ListItem</b>(s) 
                    - <b>Card</b> <i>(displays main car info)</i>
                    - <b>Modal</b> <i>(displays all car info)</i>


# Main Components

## DataStoreProvider
Provider of a MobX store containing a dark theme boolean setter functions for this boolean.

## ApolloProvider
ApolloProvider provides an ApolloClient, enabling communication to the Apollo server.

## NavBar
Navigation bar containing a title and a dropdown for changing between light and dark mode. The dropdown is hidden inside a burger meny for smaller screens.

## CarList
The CarList-component includes a filtration menu as well as a list-view displaying the cars from the search.

### Filtration menu
In this menu, the client can search by brand or model, and select minimum and maximum model year.

The query is executed when clicking the search-button, or by pressing the enter key. 

### List of cars
The list can be sorted on the fields ```Brand```, ```Model``` and ```Price```, this will trigger a query for sorted data.
In other words, all sorting is done on the backend, on the whole dataset.

The list includes ListItems(s).

#### ListItem
The ListItem-component shows main information about a car, i.e. the brand, model and price.

When a ListItem is clicked, a modal is launched.
The modal displays all information about the car, and includes functionality for liking the car.


# Use of

## Apollo
The project uses a local apollo-express server with a GraphQL schema, see the server-file ```server.ts```.

When the server is running, the GrapiQL playground can be reached at ```localhost:4000/graphql```.

### GraphQL
The GraphQL schema includes type definitions and resolvers.

Definitions are located in the server-file ```resolvers.ts```, and type definitions in ```typedefs.ts```.

#### Query-example
- Get all cars matching a filter and sort:
```javascript
cars(
  filter: {brand: "ford" model: "f-150" minyear: 2010 maxyear: 2014, page: 1}
  sort: {field: price order: ASC}
){
  cars{
  brand
  model
  year
  price
  }
  finalpage
}
```
Filter and all its variables are optional, the default variable values are: min/maxyear-variables 0/2020, brand & model "" and page 1.

Sort is optional, the variable field is mandatory and order is optional, default order is ASC (ascendingly). Sortable fields are brand, model, price, year and likes.

We originally had a separate query for getting the final page, but it was merged into the cars-query.
We merged the queries because we realized that, with our implementation, the client needs to fetch the final page on every car fetch, so two queries would be excessive and lead to more code and rerenders.
In addition, the server 'stress' would be double, and the database would have to filter on the cars two times, which is not scalable.

#### Mutation-example
Like/dislike a car based on mandatory id and likeAmount (like: 1, remove like: -1): 
```javascript
mutation like{
  like(id: 3 likeAmount: 1){
    success
  }
}
```

### MongoDB
The Apollo-server communicates, through Mongoose, with a MongoDB-database.

This database holds the entire car sale dataset.

### MobX
MobX is used to switch between light and dark theme. The store contains a boolean as well as functions for updating it.

### LocalStorage
LocalStorage is used to store whether the client has liked a car or not.
Since we did not implement user-functionality with login, localstorage is useful as it 'remembers' what cars have been liked, and therefore the client is unable to like a car multiple times.

What theme (dark/light) the user has selected is also stored in localstorage.

### Reactstrap
The project contains some components from [React Bootstrap 4](https://reactstrap.github.io/).

The most relevant ones are Row, Col, Pagination, Modal and Card. These are great because they provide som functionality and styling 'out of the box'.

# Responsiveness
The main list-view, aswell as the filter inputs and sorting buttons, are stretched as the window size changes. In addition to this we added a few media queries to adjust the margin of the list-view, so it doesn't get too wide on a full screen. All of the components fit in the full screen on desktop without having to scroll the website. The website also scales to fit smaller screens and mobile phones.


# Testing
The server-file ```App.test.tsx``` runs a snapshot-test of <b>App</b>

The client-file ```cypress/integration/clientTest.ts``` contains ent-to-end-testing of the client using [Cypress](https://www.cypress.io/).
