export function createStore() {
  return {
      darkTheme: false,
      setDarkTheme() {
        this.darkTheme = true;
      },
      setLightTheme() {
        this.darkTheme = false;
      }
  };
}
export type TStore = ReturnType<typeof createStore>;