import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

it('matches snapshot', () => {
  const tree = render(<App></App>);

  expect(tree).toMatchSnapshot();
});
