import React from 'react';
import { createStore, TStore } from './store';
import { useLocalObservable} from 'mobx-react-lite';
//useLocalStore as in the org exapmple is deprecated and replaced by useLocalObservable

const StoreContext = React.createContext<TStore | null>(null);

export const DataStoreProvider = ({ children }: any) => {
    //store is made observable
    const store = useLocalObservable(createStore);
    return <StoreContext.Provider value={store}>{children}</StoreContext.Provider>;
};

export const useDataStore = () => {
    const store = React.useContext(StoreContext);
    if (!store) {
        throw new Error('useStore must be used within a StoreProvider.');
    }
    return store;
};