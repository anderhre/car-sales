import React, { FC, ReactNode } from 'react';
import './App.css';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from 'apollo-boost';
import CarList from './components/CarList';
import NavBar from './components/NavBar';
import { DataStoreProvider } from './context';

export type StoreComponent = FC<{
  children: ReactNode;
}>;

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql',
});

const App: FC = () => {
  return (
    <DataStoreProvider>
      <ApolloProvider client={client}>
        <div className="App">
          <NavBar />
          <CarList />
        </div>
      </ApolloProvider>
    </DataStoreProvider>
  );
};

export default App;
