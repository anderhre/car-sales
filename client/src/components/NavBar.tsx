import './css/NavBar.css';
import React, { FC, useState } from 'react';
import { useDataStore } from '../context';
import { observer } from 'mobx-react-lite';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';

const NavBar: FC = observer(() => {
  const [isOpen, setIsOpen] = useState(false);
  const store = useDataStore();

  localStorage.getItem('darkTheme') === 'true' || null
    ? store.setDarkTheme()
    : store.setLightTheme();

  function darkTheme() {
    store.setDarkTheme();
    localStorage.setItem('darkTheme', 'true');
  }
  function lightTheme() {
    store.setLightTheme();
    localStorage.setItem('darkTheme', 'false');
  }

  return (
    <div>
      <Navbar fixed="top" color="dark" light expand="md">
        <NavbarBrand href="/">
          <div className="dark-nav">Car sales</div>
        </NavbarBrand>
        <NavbarToggler
          onClick={() => {
            setIsOpen(!isOpen);
          }}
        />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                <b className="dark-nav">Theme</b>
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem onClick={() => lightTheme()}>Light</DropdownItem>
                <DropdownItem onClick={() => darkTheme()}>Dark</DropdownItem>
                <DropdownItem divider />
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
});

export default NavBar;
