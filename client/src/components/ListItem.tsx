import './css/ListItem.css';
import React from 'react';
import { observer } from 'mobx-react-lite';
import { Card, CardText, CardBody, CardTitle, Row, Col } from 'reactstrap';

const ListItem = observer((car: any) => {
  return (
    <div>
      <Card>
        <CardBody>
          <Row>
            <Col>
              <CardText>{car.props.brand}</CardText>
            </Col>
            <Col>
              <CardTitle>{car.props.model}</CardTitle>
            </Col>
            <Col>
              <CardText>{car.props.price}</CardText>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </div>
  );
});

export default ListItem;
