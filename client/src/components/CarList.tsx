import './css/CarList.css';
import React, { FC, useEffect, useState } from 'react';
import gql from 'graphql-tag';
import ListItem from './ListItem';
import { useDataStore } from '../context';
import { observer } from 'mobx-react-lite';
import { useMutation } from 'react-apollo';
import { useLazyQuery } from 'react-apollo';
import {
  Row,
  Col,
  Input,
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  ListGroupItem,
  Badge,
  Pagination,
  PaginationItem,
  PaginationLink,
} from 'reactstrap';

interface Car {
  index: number;
  price: number;
  model: String;
  brand: String;
  year: number;
  title_status: String;
  mileage: number;
  color: String;
  vin: String;
  lot: number;
  state: string;
  country: String;
  condition: String;
  likes: Number;
}

const LIKE_MUTATION = gql`
  mutation RatingMutation($id: Int!, $likeAmount: Int!) {
    like(id: $id, likeAmount: $likeAmount) {
      success
    }
  }
`;

const CARS_QUERY = gql`
  query CarsQuery(
    $brand: String
    $model: String
    $minYear: Int
    $maxYear: Int
    $sortField: SortableField!
    $sortOrder: SortOrder
    $page: Int
  ) {
    cars(
      filter: { brand: $brand, model: $model, minyear: $minYear, maxyear: $maxYear, page: $page }
      sort: { field: $sortField, order: $sortOrder }
    ) {
      cars {
        index
        price
        model
        brand
        year
        title_status
        mileage
        color
        vin
        lot
        state
        country
        condition
        likes
      }
      finalpage
    }
  }
`;

let currentCar: any = {
  index: 0,
  price: 0,
  model: 'model',
  brand: 'brand',
  year: 0,
  title_status: 'title_status',
  mileage: 0,
  color: 'color',
  vin: 'vin',
  lot: 0,
  state: 'state',
  country: 'country',
  condition: 'condition',
  likes: 0,
};

const FIRST_PAGE = 1; // Used when fetching first page
let sortField = 'brand'; // Default field to sort on
let sortOrder = 'ASC'; // Default sort-order

const CarList: FC = observer(() => {
  // State hooks
  const [page, setPage] = useState(1);
  const [finalPage, setFinalPage] = useState(0);
  const [brand, setBrand] = useState('');
  const [model, setModel] = useState('');
  const [minYear, setMinYear] = useState(1973);
  const [maxYear, setMaxYear] = useState(2020);
  const [modal, setModal] = useState(false);
  const [getCars, { data }] = useLazyQuery(CARS_QUERY);
  const [insertLike] = useMutation(LIKE_MUTATION);

  // MobX store and theme variables
  const store = useDataStore();
  const theme = store.darkTheme ? 'dark-car-list' : 'light-car-list';
  const selectTheme = store.darkTheme ? 'dark-select' : 'light-select';
  const textColor = store.darkTheme ? 'dark-text' : 'light-text';
  const buttonColor = store.darkTheme ? 'danger' : 'secondary';
  const buttonColorSearch = store.darkTheme ? 'danger' : 'secondary';

  // Modal variables
  const id = currentCar.index;
  let likeButtonColor: string = '';
  let badgeColor: string = '';
  let likeText = '';
  const likeStatus = localStorage.getItem(id);

  // Search (query) for cars
  function search(e: any) {
    if (e.key === 'Enter') {
      setPage(1);
      getCars({
        variables: {
          brand,
          model,
          minYear,
          maxYear,
          sortField,
          sortOrder,
          FIRST_PAGE,
        },
      });
    }
  }

  // Handle click on page-buttons
  function handlePageClick(page: number) {
    getCars({
      variables: {
        brand,
        model,
        minYear,
        maxYear,
        sortField,
        sortOrder,
        page,
      },
    });

    // If final page-button was clicked
    if (page === -1) {
      setPage(finalPage);
    } else {
      setPage(page);
    }
  }

  // Handle click on table-headers
  function handleHeaderClick(header: string) {
    resetHeaderText();
    sortField = header;

    // Change sort order
    sortOrder === 'ASC' ? (sortOrder = 'DESC') : (sortOrder = 'ASC');

    // Change header text (^/v based on sort order)
    let btn = document.getElementsByClassName(header + '-button')[0];
    header = header.charAt(0).toUpperCase() + header.slice(1);
    btn.innerHTML = sortOrder === 'ASC' ? header + ' ^' : header + ' v';

    // Search for cars
    search({ key: 'Enter' });
  }

  // Reset text of all table-headers
  function resetHeaderText() {
    let headers = ['Brand', 'Model', 'Price'];
    let classNames = ['brand-button', 'model-button', 'price-button'];

    for (let i = 0; i < 3; i++) {
      let btn = document.getElementsByClassName(classNames[i])[0];
      btn.innerHTML = headers[i];
    }
  }

  // Set final page when data arrives from server
  useEffect(() => {
    if (data !== undefined) {
      setFinalPage(data.cars.finalpage);
    }
  }, [data]);

  // Toggles Modal and sets currentCar
  function openModal(car: any) {
    currentCar = car;
    setModal(!modal);
  }

  // Change UI modal variables based on localstorage value
  if (likeStatus === 'disliked' || likeStatus === null) {
    likeButtonColor = 'success';
    badgeColor = 'secondary';
    likeText = 'Like';
  } else {
    likeButtonColor = 'danger';
    badgeColor = 'success';
    likeText = 'Dislike';
  }

  function likeToggle() {
    let likeAmount = 0;

    // Has not liked or disliked this car before
    if (localStorage.getItem(id) === null) {
      likeAmount = 1;
      localStorage.setItem(id, 'liked');
      // This car was liked
    } else if (localStorage.getItem(id) === 'liked') {
      likeAmount = -1;
      localStorage.setItem(id, 'disliked');
      //This car was disliked
    } else if (localStorage.getItem(id) === 'disliked') {
      likeAmount = 1;
      localStorage.setItem(id, 'liked');
    }

    insertLike({
      variables: {
        id,
        likeAmount,
      },
    });

    currentCar.likes += likeAmount;
    setModal(true); // Rerender
  }

  return (
    <div className={theme}>
      <Col>
        <div className="filterWrapper">
          <Row>
            <Col>
              <Col>
                <div className={textColor}>
                  <b>Brand</b>
                  <Input
                    className="brand-input-field"
                    name="search"
                    id="search-brand"
                    placeholder="Brand"
                    onChange={(e) => setBrand(e.target.value)}
                    onKeyDown={search}
                  />
                </div>
              </Col>
              <Col>
                <div className={textColor}>
                  <b>Model</b>
                  <Input
                    className="model-input-field"
                    name="search"
                    id="search-model"
                    placeholder="Model"
                    onChange={(e) => setModel(e.target.value)}
                    onKeyDown={search}
                  />
                </div>
              </Col>
            </Col>
            <Col>
              <Col>
                <div className={textColor}>
                  <b>Min year</b>
                  <select
                    className={selectTheme}
                    name="minyear-combo"
                    id="minyear"
                    onChange={(e) => setMinYear(parseInt(e.target.value))}
                    defaultValue={1973}
                  >
                    {getYearOptions(1973, maxYear)}
                  </select>
                </div>
              </Col>
              <Col>
                <div className={textColor}>
                  <b>Max year</b>

                  <select
                    className={selectTheme}
                    name="maxyear-combo"
                    id="maxyear"
                    onChange={(e) => setMaxYear(parseInt(e.target.value))}
                    defaultValue={2020}
                  >
                    {getYearOptions(minYear, 2020)}
                  </select>
                </div>
              </Col>
            </Col>
          </Row>
        </div>
        <div id="button-wrapper">
          <Row>
            <Col></Col>
            <Button
              color={buttonColorSearch}
              id="search-btn"
              onClick={() => {
                getCars({
                  variables: {
                    brand,
                    model,
                    minYear,
                    maxYear,
                    sortField,
                    sortOrder,
                    FIRST_PAGE,
                  },
                });
                resetHeaderText();
                setPage(1);
              }}
            >
              Search
            </Button>
            <Col></Col>
          </Row>
        </div>
      </Col>
      <Col>
        <div className="car-table-headers">
          <Row>
            <Col id="filter-columns">
              <Button
                color={buttonColor}
                className="brand-button"
                onClick={() => handleHeaderClick('brand')}
              >
                <div>Brand</div>
              </Button>
            </Col>
            <Col id="filter-columns">
              <Button
                color={buttonColor}
                className="model-button"
                onClick={() => handleHeaderClick('model')}
              >
                <div>Model</div>
              </Button>
            </Col>
            <Col id="filter-columns">
              <Button
                color={buttonColor}
                className="price-button"
                onClick={() => handleHeaderClick('price')}
              >
                <div>Price</div>
              </Button>
            </Col>
          </Row>
        </div>

        <div className={'car-table'}>
          {data !== undefined && data.cars.cars.length === 0 && <div>No cars match the search</div>}
          {data !== undefined &&
            data.cars.cars.map(function (car: Car, index: number) {
              return (
                <div className="list-item-wrapper" key={index} onClick={() => openModal(car)}>
                  <ListItem key={index} props={car}></ListItem>
                </div>
              );
            })}
        </div>
      </Col>

      <Col>
        <div className="car-table-pagination">
          <Row>
            <Col></Col>
            {data !== undefined && data.cars.cars.length !== 0 && (
              <Pagination aria-label="Page navigation">
                {page !== 1 && (
                  <>
                    <PaginationItem onClick={() => handlePageClick(1)}>
                      <PaginationLink first className="page-first" />
                    </PaginationItem>
                    <PaginationItem onClick={() => handlePageClick(page - 1)}>
                      <PaginationLink previous />
                    </PaginationItem>
                  </>
                )}
                <PaginationItem>
                  <PaginationLink>{page}</PaginationLink>
                </PaginationItem>
                {page !== finalPage && (
                  <>
                    <PaginationItem onClick={() => handlePageClick(page + 1)}>
                      <PaginationLink next />
                    </PaginationItem>
                    <PaginationItem onClick={() => handlePageClick(-1)}>
                      <PaginationLink last />
                    </PaginationItem>
                  </>
                )}
              </Pagination>
            )}
            <Col></Col>
          </Row>
        </div>
      </Col>

      {modal && (
        <div className="modal-wrapper">
          <Modal isOpen={modal} toggle={() => setModal(!modal)}>
            <ModalHeader toggle={() => setModal(!modal)}>
              <div className="model-title">
                <h2>
                  {currentCar.brand} - {currentCar.model}
                </h2>
              </div>
              <div>
                Likes: <Badge color={badgeColor}>{currentCar.likes}</Badge>
              </div>
            </ModalHeader>
            <ModalBody>
              <ListGroupItem color="secondary">
                <b>Location: </b> {currentCar.state}, {currentCar.country}
              </ListGroupItem>
              <ListGroupItem color="secondary">
                <b>Mileage: </b>
                {currentCar.mileage}
              </ListGroupItem>
              <ListGroupItem color="secondary">
                <b>Year: </b>
                {currentCar.year}
              </ListGroupItem>
              <ListGroupItem color="secondary">
                <b>Color: </b>
                {currentCar.color}
              </ListGroupItem>
              <ListGroupItem color="secondary">
                <b>Vin: </b>
                {currentCar.vin}
              </ListGroupItem>
            </ModalBody>
            <ModalFooter>
              <Button color={likeButtonColor} onClick={likeToggle}>
                {likeText}
              </Button>
              <Button
                color="secondary"
                onClick={() => {
                  setModal(!modal);
                }}
              >
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        </div>
      )}
    </div>
  );
});

// Get numbers for selects (combo-boxes)
function getYearOptions(start: number, end: number) {
  let options = [];
  for (let i = start; i <= end; i++) {
    options.push(
      <option key={i} value={i}>
        {i}
      </option>,
    );
  }
  return options;
}

export default CarList;
