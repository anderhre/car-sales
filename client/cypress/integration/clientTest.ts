/// <reference types="cypress" />
import { PAGESIZE } from "../../../server/resolvers";

describe('clientTest', () => {

  beforeEach(() => {
    cy.visit("http://localhost:3000");
  })

  it('Sees text for filtration options and Search button', () => {
    cy.contains("Min year");
    cy.contains("Max year");
    cy.contains("1973");
    cy.contains("2020");
    cy.contains("Search");
  })

  it('displays cars when Search is clicked', () => {
    // Assert no cars are displayed
    cy.get(".card-body").should("have.length", 0);

    // Click Search and assert PAGESIZE cars are displayed
    cy.contains("Search").click();
    cy.get(".card-body").should("have.length", PAGESIZE);
  })

  it('filters correctly', () => {

    // Search cars and assert PAGESIZE cars are displayed
    cy.contains("Search").click();
    cy.get(".card-body").should("have.length", PAGESIZE);

    // Write brand and model
    cy.get(".brand-input-field").type("ford");
    cy.get(".model-input-field").type("focus");
    
    // Assert text is written
    cy.get(".brand-input-field").should("have.value", "ford");
    cy.get(".model-input-field").should("have.value", "focus");

    // Search cars and assert less cars are displayed
    cy.get(".model-input-field").type('{enter}')
    cy.get(".card-body").should("have.length.below", PAGESIZE);
    
    // Assert more than 6 cars are displayed
    cy.get(".card-body").should("have.length.above", 6);

    // Choose min model year
    cy.get("#minyear").select("2018");
    cy.get("#minyear").should("have.value", 2018);
    cy.contains("Search").click()

    // Assert less than 6 cars are displayed
    cy.get(".card-body").should("have.length.below", 6);
  })
})
